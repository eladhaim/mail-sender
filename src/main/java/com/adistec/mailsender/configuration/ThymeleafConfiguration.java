package com.adistec.mailsender.configuration;

import java.util.HashSet;
import java.util.Set;
import org.apache.commons.codec.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.dialect.SpringStandardDialect;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;

@Configuration
public class ThymeleafConfiguration {

    @SuppressWarnings("unused")
    private final Logger log = LoggerFactory.getLogger(ThymeleafConfiguration.class);
    
    @Bean
    public ITemplateResolver fileTemplateResolver() {
        ClassLoaderTemplateResolver emailTemplateResolver = new ClassLoaderTemplateResolver();
        emailTemplateResolver.setPrefix("templates/");
        emailTemplateResolver.setSuffix(".html");
        emailTemplateResolver.setTemplateMode("HTML5");
        emailTemplateResolver.setCharacterEncoding(CharEncoding.UTF_8);
        emailTemplateResolver.setOrder(1);
        emailTemplateResolver.setCheckExistence(true);
        return emailTemplateResolver;
    }
    
    @Bean
    public TemplateEngine templateEngine () {
    	SpringTemplateEngine templateEngine = new SpringTemplateEngine();
    	Set<ITemplateResolver> templateResolvers = new HashSet<ITemplateResolver>();
    	templateResolvers.add(stringTemplateResolver());
    	templateResolvers.add(fileTemplateResolver());
    	templateEngine.setTemplateResolvers(templateResolvers);
        SpringStandardDialect dialect = new SpringStandardDialect();
    	dialect.setEnableSpringELCompiler(true);
        return templateEngine;
    }
    
    @Bean
    public ITemplateResolver stringTemplateResolver() {
        StringTemplateResolver templateResolver = new StringTemplateResolver();
        templateResolver.setCacheable(false);
        templateResolver.setTemplateMode("HTML5");
        templateResolver.setOrder(2);
        return templateResolver;
    }
}