package com.adistec.mailsender.service.Impl;

import org.apache.commons.codec.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import com.adistec.mailsender.service.MailService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MailServiceImpl implements MailService {
	
	@Autowired
    private TaskExecutor taskExecutor;
	
    @Value("${spring.mail.account.devsend}")
    private String emailFrom;
    
    @Value("${spring.mail.account.webdev}")
    private String webDevMail;
    
    @Value("#{'${spring.mail.account.feedback}'.split(',')}") 
    private String[] feedbackTo;
    
    @Value("${spring.mail.template.default}")
    private String defaultTemplate;
    
    @Value("${spring.mail.properties.feedbackSubject}")
    private String feedbackSubject;
	
	@Value("${spring.mail.account.devsend}")
    private String devsendAccount;
	
	@Value("${spring.mail.account.noreply}")
    private String noreply;
	
	@Value("${spring.mail.template.recovery}")
    private String recoveryTemplate;
	
	@Value("${spring.mail.template.feedback}")
    private String feedbackTemplate;
	
	@Value("${spring.profiles.active}")
	private String activeProfile;
	
    @Autowired
    private JavaMailSender emailSender;
    
    @Autowired
    private SpringTemplateEngine templateEngine;
    
    public void sendEmail(String[] to, String from, String subject, boolean isMultipart, boolean isHtml, HashMap<String,Resource> inlines, String templateVariables, String template, String[] copyTo, String[] cco) throws MessagingException, JsonParseException, JsonMappingException, IOException {
    	if (to == null) {
    		to = new String[] {webDevMail};
    	}
    	if (from == null)
    		from = emailFrom;
    	String content = setContent(inlines, templateVariables, template);
    	MimeMessage mimeMessage = setMimeMessage(to, from, subject, isMultipart, isHtml, inlines, content, copyTo, cco);
        taskExecutor.execute( new Runnable() {
    	   public void run() {
    		   emailSender.send(mimeMessage);
    	   }
        });
    }
    
    @Override
	public void sendEmailWithTemplate(String sentToEmail, String subject, String template, String from,
			String templateVariables, String inlines, String copyTo, String cco) throws JsonParseException, JsonMappingException, IOException, MessagingException {
    	String[] sentToEmailArray = parseStringToStringArray(sentToEmail);
    	String[] copyToArray = parseStringToStringArray(copyTo);
    	String[] ccoArray = parseStringToStringArray(cco);
		HashMap<String, String> inlinesStringMap = parseStringToHashMap(inlines);
		HashMap<String, Resource> inlinesResourceMap = null;
		if (inlinesStringMap != null) {
			inlinesResourceMap = parseInlinesMap(inlinesStringMap);
		}
        sendEmail(sentToEmailArray, from, subject, inlines != null, true, inlinesResourceMap, templateVariables, template, copyToArray, ccoArray);
	}
    
    @SuppressWarnings("unchecked")
	private String[] parseStringToStringArray(String source) throws JsonParseException, JsonMappingException, IOException {
    	return source != null ? (String[]) new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).readValue(source, new TypeReference<String[]>() {}) : null;
	}

	@Override
	public void sendFeedback(String templateVariables, String appName, String subject) throws MessagingException, JsonParseException, JsonMappingException, IOException {
    	String logoPath = "static/images/" + appName + ".png";
    	HashMap<String, Resource> inlines = null;
		inlines = setInlineFromPath(inlines, logoPath, "logo");
		String[] to = new String[0];
		sendEmail(feedbackTo, devsendAccount, subject, inlines != null, true,inlines, templateVariables, feedbackTemplate, null, null);
	}
    
	@Override
	public void sendRecovery(String templateVariables, String subject, String username) throws MessagingException, JsonParseException, JsonMappingException, IOException {
		String blueLogo = "static/images/adistec-logo-blue.png";
		HashMap<String, Resource> inlines = null;
		inlines = setInlineFromPath(inlines, blueLogo, "blueLogo");
		sendEmail(new String[] {username}, noreply, subject, inlines != null, true,inlines, templateVariables, recoveryTemplate, null, null);
	}

	private HashMap<String, Resource> setInlineFromPath(HashMap<String, Resource> inlines, String path, String key) {
		if (this.getClass().getClassLoader().getResource(path) != null) {
			inlines = new HashMap<String, Resource>();
			inlines.put(key, new ClassPathResource(path));
		}
		return inlines;
	}
    
    public List<String> getTemplateFiles() throws IOException  {
    	List<String> templates = new ArrayList<String>();
    	ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
    	for (Resource resource: resolver.getResources("classpath*:/templates/*.html")){
    	    templates.add(resource.getFilename());
    	}
    	return templates;
    }
	
	@SuppressWarnings("unchecked")
	private HashMap<String, String> parseStringToHashMap(String source) throws JsonParseException, JsonMappingException, IOException {
		return source != null ? (HashMap<String, String>) new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).readValue(source, HashMap.class) : null;
	}
	
	private HashMap<String, Resource> parseInlinesMap(HashMap<String, String> inlinesStringMap) {
		HashMap<String, Resource> inlinesResourceMap = new HashMap<String, Resource>();
		for (Map.Entry<String, String> entry : inlinesStringMap.entrySet()) {
			inlinesResourceMap.put(entry.getKey(), getResourceFromBase64String(entry.getValue()));
		}
		return inlinesResourceMap;
	}
	
	private Resource getResourceFromBase64String(String input) {
		ByteArrayResource resource;
		byte[] byteA = Base64.getDecoder().decode(input);
		resource = new ByteArrayResource(byteA);
		return resource;
	}
	
	private void setContext(Context ctx, HashMap<String, String> variables, HashMap<String, Resource> inlines) {
    	if (variables != null) {
    		variables.entrySet().stream().forEach(variable -> ctx.setVariable(variable.getKey(),variable.getValue()));
        }
        if(inlines != null) {
            inlines.entrySet().stream().forEach(inline ->
            ctx.setVariable(inline.getKey(), inline.getKey())
            );
        }
    }
	
	private MimeMessage setMimeMessage(String[] to, String from, String subject, boolean isMultipart, boolean isHtml,
			HashMap<String, Resource> inlines, String content, String[] copyTo, String[] cco) throws MessagingException {
		MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
        message.setTo(to);
        message.setFrom(from);
        message.setSubject(subject);
        message.setText(content, isHtml);
        if (copyTo != null) 
        	message.setCc(copyTo);
        if (cco != null) 
        	message.setBcc(cco);
        if(inlines != null) {
            for (Map.Entry<String, Resource> entry : inlines.entrySet()) {
				message.addInline(entry.getKey(), entry.getValue(), "image/png");
            }
        }
		return mimeMessage;
	}

	private String setContent(HashMap<String, Resource> inlines, String templateVariables, String template)
			throws IOException, JsonParseException, JsonMappingException {
		HashMap<String, String> variablesMap = parseStringToHashMap(templateVariables);
    	Context context = new Context();
    	setContext(context, variablesMap, inlines);
    	String content = templateEngine.process(template, context);
		return content;
	}
}